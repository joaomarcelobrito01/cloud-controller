'use strict';

const
    mongoose            = require('mongoose'),
    bcrypt              = require('bcrypt');

let UserSchema = new mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    }
}, { timestamps: true });

UserSchema.methods.hashPassword = function hashPassword() {
    let salt = bcrypt.genSaltSync(10);
    this.password = bcrypt.hashSync(this.password, salt);
}

UserSchema.methods.comparePassword = function comparePassword(password) {
    return bcrypt.compareSync(password, this.password);
}

mongoose.model('User', UserSchema);