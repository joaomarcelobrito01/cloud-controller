'use strict';

const
    mongoose            = require('mongoose'),
    config              = require('../../config.json');

mongoose.connect(config.db.url);
mongoose.Promise = Promise;

require('./user');