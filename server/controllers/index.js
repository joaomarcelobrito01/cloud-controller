'use strict';

class Controllers {
    constructor(opts) {
        if (!opts) opts = {};

        if (!opts.routes) opts.routes = './routes.json';
        if (!opts.app) throw "Express app is obligated";

        this.app = opts.app;

        try {
            let routes = require(opts.routes);

            for (let each of routes) {
                let Controller = require(each.file);
                let controller = new Controller();
                
                for (let route of each.routes) {
                    switch (route.type) {
                        case 'action':
                            this.app[route.method](`${each.path}/${route.action}`, controller[route.action].bind(controller));
                        break;

                        case 'render':
                            this.app[route.method](`${each.path}${route.view}`, function onView(req, res) {
                                let params = controller[route.view](req);
                                res.render(`${route.view}.ejs`, params);
                            });
                        break;

                        default: throw "INVALID TYPE " + route.type;
                    }
                }
            }

        } catch (e) {
            console.warn(`[ IMPORT ERROR ] ${e.stack}`);
            throw e.stack;
        }
    }
}

module.exports = Controllers;