'use strict';

const
    Controller      = require('./controller'),
    mongoose        = require('mongoose');

const
    User            = mongoose.model('User');

class UserController extends Controller {
    constructor() {
        super();
    }

    auth(req, res) {
        let check = Controller.argsAllowed(req.body, ['email', 'password']);
        if (!check) {
            res.send('Argument not allowed.');
        }

        User.findOne({email: req.body.email}).then(function (user) {
            if (user) {
                if (user.comparePassword(req.body.password)) {
                    req.session.user = user;
                    req.session.save(function() {
                        res.redirect('/dashboard');
                    });
                } else {
                    //Password mismatch
                    res.send('Password mismatch');
                }
            } else {
                //User dont exist
                res.send('User dont exist');
            }
        });
    }

    signup(req, res) {
        let check = Controller.argsAllowed(req.body, ['name', 'email', 'password']);
        if (!check) {
            //TODO: Create error handler
            res.send('Argument not allowed.');
        }

        let user = new User({
            name: req.body.name,
            email: req.body.email,
            password: req.body.password
        });

        user.hashPassword();

        user.save(function (err, user) {
            if (err) res.send(err);
            else {
                req.session.user = user;
                res.redirect('/dashboard');
            }
        });
    }   
}

module.exports = UserController;