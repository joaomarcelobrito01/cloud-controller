'use strict';

const
    config          = require('../../config.json'),
    SystemInfo      = require('../../util/system_info');

class PagesController {
    constructor() {

    }

    login() {
        let params = {
            canSignUp: config.canSignUp
        };

        return params;
    }

    signup() {
        return {};
    }

    dashboard(req) {
        //TODO: Check if signed in
        let params = {
            user: req.session.user,
            systemInfo: SystemInfo.getSystemInfo()
        };

        return params;
    }
}

module.exports = PagesController;