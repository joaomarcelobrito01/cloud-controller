'use strict';

class Controller {
    constructor() {

    }

    static argsAllowed(args, args_needed) {
        for (let each in args) {
            if (args_needed.indexOf(each) === -1) return false;
        }
        return true;
    }
}

module.exports = Controller