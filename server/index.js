'use strict';

const
    EventEmitter            = require('events'),
    express                 = require('express'),
    session                 = require('express-session'),
    bodyParser              = require('body-parser'),
    http                    = require('http'),
    io                      = require('socket.io'),
    models                  = require('./models'),
    Controllers             = require('./controllers');

class Server extends EventEmitter {
    constructor(opts) {
        super();

        if (!opts) opts = {};
        if (!opts.port) opts.port = 3001;
        this.opts = opts;
        
        this.app = express();
        this.server = http.createServer(this.app);
        this.socket = io(this.server);
        this.socket.on('connection', this.onSocketConnection.bind(this));
        this.server.listen(opts.port, this.onStartListening.bind(this));

        //Sets
        this.app.set('views', `${__dirname}/views`);
        this.app.set('view engine', 'ejs');
        this.app.set('trust proxy', 1);

        //Middlewares
        this.app.use(express.static(`${__dirname}/public`));
        this.app.use(session({
            secret: '80442bc0e77691c471eedfdc44580ff1fb936353e1fbb00a91be34961f2c12d5', //Cloud_Controller in SHA256
            resave: false,
            saveUninitialized: true,
            cookie: { secure: false }
        }));
        this.app.use(bodyParser.json({limit: '15mb'}));
        this.app.use(bodyParser.urlencoded({ extended: true, limit:'15mb' }));

        //Routing
        let controllers = new Controllers({app:this.app});
    }

    sendUpdateInfo(client) {
        this.send(client, 'update info', {
            system: SystemInfo.getSystemInfo()
        });
    }

    send(client, message, args) {
        client.emit(message, args);
    }

    render(req, res, page) {
        let params = {};
        if (req.session.user) params.user = user;
        this.app.render(`${page}.ejs`, params);
    }

    onStartListening() {
        if (this.opts.debug) {
            console.log(`[ SERVER ] Started listening to port ${this.opts.port}`);
        }
    }

    onSocketConnection(client) {
        if (this.opts.debug) {
            console.log(`[ SERVER ] New user connected with id ${client.id}`);
        }

        client.on('message', this.onClientMessage.bind(this));
        client.on('disconnect', this.onClientDisconnect.bind(this));

        this.sendUpdateInfo(client);
    }

    onClientMessage(message, client) {
        if (this.opts.debug) {
            console.log(`[ SERVER ] Message received: ${message}`);
        }
    }

    onClientDisconnect(client) {
        if (this.opts.debug) {
            console.log(`[ SERVER ] Client disconnect`);
        }
    }
}

module.exports = Server;