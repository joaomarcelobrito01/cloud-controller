'use strict';

const
    program         = require('commander'),
    Server          = require('./server'),
    pkg             = require('./package.json');


program
    .version(pkg.version)
    .option('-s, --server', 'Starts the server module')
    .parse(process.argv);


if (program.server) {
    console.log(`[ SYSTEM ] Server is starting...`);
    let server = new Server({
        debug: true
    });
}