'use strict';

const
    os              = require('os'),
    formatSeconds   = require('format-seconds');

class SystemInfo {
    static getSystemInfo() {
        let cpus = os.cpus();
        let freemem = os.freemem();
        let totalmem = os.totalmem();
        let hostname = os.hostname();
        let platform = os.type(); //os.platform()
        let uptime = formatSeconds(os.uptime());


        let result = {
            cpus, freemem, totalmem, hostname, platform, uptime
        };

        return result;
    }


}

module.exports = SystemInfo;