'use strict';

class Logger {
    static systemInfo(system) {
        console.log(`[ SYSTEM INFO ] CPU count: ${system.cpus.length}`);
        
        let memoryPercentage = ( 1 - ( system.freemem / system.totalmem ) ) * 100;
        console.log(`[ SYSTEM INFO ] Memory usage: ${memoryPercentage}%`);

        console.log(`[ SYSTEM INFO ] User name: ${system.hostname}`);
        console.log(`[ SYSTEM INFO ] Platform: ${system.platform}`);
        console.log(`[ SYSTEM INFO ] Uptime: ${system.uptime} seconds`);
    }
}

module.exports = Logger;